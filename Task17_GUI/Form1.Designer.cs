﻿namespace Task17_GUI
{
    partial class CharacterCreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_insert = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_create = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label_pwr = new System.Windows.Forms.Label();
            this.textBox_pwr = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox_class = new System.Windows.Forms.GroupBox();
            this.radBut_Rogue = new System.Windows.Forms.RadioButton();
            this.radBut_Mage = new System.Windows.Forms.RadioButton();
            this.radBut_Warr = new System.Windows.Forms.RadioButton();
            this.groupBox_subClass = new System.Windows.Forms.GroupBox();
            this.radBut_sub2 = new System.Windows.Forms.RadioButton();
            this.radBut_sub1 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label_name = new System.Windows.Forms.Label();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_hp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_armor = new System.Windows.Forms.TextBox();
            this.textBox_mana = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label_error = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label_description_database = new System.Windows.Forms.Label();
            this.cBox_chars = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label_description = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox_class.SuspendLayout();
            this.groupBox_subClass.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.94722F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.05278F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_error, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1099, 510);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.tableLayoutPanel8);
            this.panel1.Controls.Add(this.tableLayoutPanel4);
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(460, 490);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Controls.Add(this.btn_delete, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.btn_insert, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.btn_update, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 430);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(460, 60);
            this.tableLayoutPanel8.TabIndex = 3;
            // 
            // btn_delete
            // 
            this.btn_delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_delete.Location = new System.Drawing.Point(309, 3);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(148, 54);
            this.btn_delete.TabIndex = 0;
            this.btn_delete.Text = "Delete";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.Btn_delete_Click);
            // 
            // btn_insert
            // 
            this.btn_insert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_insert.Location = new System.Drawing.Point(3, 3);
            this.btn_insert.Name = "btn_insert";
            this.btn_insert.Size = new System.Drawing.Size(147, 54);
            this.btn_insert.TabIndex = 1;
            this.btn_insert.Text = "Insert";
            this.btn_insert.UseVisualStyleBackColor = true;
            this.btn_insert.Click += new System.EventHandler(this.Btn_insert_Click);
            // 
            // btn_update
            // 
            this.btn_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_update.Location = new System.Drawing.Point(156, 3);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(147, 54);
            this.btn_update.TabIndex = 2;
            this.btn_update.Text = "Update";
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Click += new System.EventHandler(this.Btn_update_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btn_create, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 317);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(460, 113);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // btn_create
            // 
            this.btn_create.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_create.Location = new System.Drawing.Point(3, 59);
            this.btn_create.Name = "btn_create";
            this.btn_create.Size = new System.Drawing.Size(454, 51);
            this.btn_create.TabIndex = 12;
            this.btn_create.Text = "Create";
            this.btn_create.UseVisualStyleBackColor = true;
            this.btn_create.Visible = false;
            this.btn_create.Click += new System.EventHandler(this.Btn_create_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel5.Controls.Add(this.label_pwr, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBox_pwr, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(460, 49);
            this.tableLayoutPanel5.TabIndex = 0;
            this.tableLayoutPanel5.Visible = false;
            // 
            // label_pwr
            // 
            this.label_pwr.AutoSize = true;
            this.label_pwr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_pwr.Location = new System.Drawing.Point(3, 0);
            this.label_pwr.Name = "label_pwr";
            this.label_pwr.Size = new System.Drawing.Size(109, 49);
            this.label_pwr.TabIndex = 10;
            this.label_pwr.Text = "specialPower";
            this.label_pwr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_pwr
            // 
            this.textBox_pwr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_pwr.Location = new System.Drawing.Point(125, 10);
            this.textBox_pwr.Margin = new System.Windows.Forms.Padding(10);
            this.textBox_pwr.Name = "textBox_pwr";
            this.textBox_pwr.Size = new System.Drawing.Size(325, 26);
            this.textBox_pwr.TabIndex = 11;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.groupBox_class, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox_subClass, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 187);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(460, 130);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // groupBox_class
            // 
            this.groupBox_class.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox_class.Controls.Add(this.radBut_Rogue);
            this.groupBox_class.Controls.Add(this.radBut_Mage);
            this.groupBox_class.Controls.Add(this.radBut_Warr);
            this.groupBox_class.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_class.Location = new System.Drawing.Point(13, 3);
            this.groupBox_class.Name = "groupBox_class";
            this.groupBox_class.Size = new System.Drawing.Size(219, 124);
            this.groupBox_class.TabIndex = 8;
            this.groupBox_class.TabStop = false;
            this.groupBox_class.Text = "Class";
            // 
            // radBut_Rogue
            // 
            this.radBut_Rogue.AutoSize = true;
            this.radBut_Rogue.Dock = System.Windows.Forms.DockStyle.Top;
            this.radBut_Rogue.Location = new System.Drawing.Point(3, 75);
            this.radBut_Rogue.Name = "radBut_Rogue";
            this.radBut_Rogue.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.radBut_Rogue.Size = new System.Drawing.Size(213, 29);
            this.radBut_Rogue.TabIndex = 2;
            this.radBut_Rogue.TabStop = true;
            this.radBut_Rogue.Text = "Rogue";
            this.radBut_Rogue.UseVisualStyleBackColor = true;
            this.radBut_Rogue.CheckedChanged += new System.EventHandler(this.RadioButton3_CheckedChanged);
            // 
            // radBut_Mage
            // 
            this.radBut_Mage.AutoSize = true;
            this.radBut_Mage.Dock = System.Windows.Forms.DockStyle.Top;
            this.radBut_Mage.Location = new System.Drawing.Point(3, 46);
            this.radBut_Mage.Name = "radBut_Mage";
            this.radBut_Mage.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.radBut_Mage.Size = new System.Drawing.Size(213, 29);
            this.radBut_Mage.TabIndex = 1;
            this.radBut_Mage.TabStop = true;
            this.radBut_Mage.Text = "Mage";
            this.radBut_Mage.UseVisualStyleBackColor = true;
            this.radBut_Mage.CheckedChanged += new System.EventHandler(this.RadBut_Mage_CheckedChanged);
            // 
            // radBut_Warr
            // 
            this.radBut_Warr.AutoSize = true;
            this.radBut_Warr.Dock = System.Windows.Forms.DockStyle.Top;
            this.radBut_Warr.Location = new System.Drawing.Point(3, 22);
            this.radBut_Warr.Name = "radBut_Warr";
            this.radBut_Warr.Size = new System.Drawing.Size(213, 24);
            this.radBut_Warr.TabIndex = 0;
            this.radBut_Warr.TabStop = true;
            this.radBut_Warr.Text = "Warrior";
            this.radBut_Warr.UseVisualStyleBackColor = true;
            this.radBut_Warr.CheckedChanged += new System.EventHandler(this.RadBut_Warr_CheckedChanged);
            // 
            // groupBox_subClass
            // 
            this.groupBox_subClass.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox_subClass.Controls.Add(this.radBut_sub2);
            this.groupBox_subClass.Controls.Add(this.radBut_sub1);
            this.groupBox_subClass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_subClass.Location = new System.Drawing.Point(238, 3);
            this.groupBox_subClass.Name = "groupBox_subClass";
            this.groupBox_subClass.Size = new System.Drawing.Size(219, 124);
            this.groupBox_subClass.TabIndex = 9;
            this.groupBox_subClass.TabStop = false;
            this.groupBox_subClass.Text = "Subclass";
            this.groupBox_subClass.Visible = false;
            // 
            // radBut_sub2
            // 
            this.radBut_sub2.AutoSize = true;
            this.radBut_sub2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radBut_sub2.Location = new System.Drawing.Point(3, 46);
            this.radBut_sub2.Name = "radBut_sub2";
            this.radBut_sub2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.radBut_sub2.Size = new System.Drawing.Size(213, 29);
            this.radBut_sub2.TabIndex = 1;
            this.radBut_sub2.TabStop = true;
            this.radBut_sub2.Text = "Subclass 2";
            this.radBut_sub2.UseVisualStyleBackColor = true;
            this.radBut_sub2.CheckedChanged += new System.EventHandler(this.RadBut_sub2_CheckedChanged);
            // 
            // radBut_sub1
            // 
            this.radBut_sub1.AutoSize = true;
            this.radBut_sub1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radBut_sub1.Location = new System.Drawing.Point(3, 22);
            this.radBut_sub1.Name = "radBut_sub1";
            this.radBut_sub1.Size = new System.Drawing.Size(213, 24);
            this.radBut_sub1.TabIndex = 0;
            this.radBut_sub1.TabStop = true;
            this.radBut_sub1.Text = "Subclass 1";
            this.radBut_sub1.UseVisualStyleBackColor = true;
            this.radBut_sub1.CheckedChanged += new System.EventHandler(this.RadBut_sub1_CheckedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel2.Controls.Add(this.label_name, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBox_name, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.textBox_hp, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBox_armor, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.textBox_mana, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(0, 20, 10, 0);
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(460, 187);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_name.Location = new System.Drawing.Point(3, 20);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(106, 41);
            this.label_name.TabIndex = 0;
            this.label_name.Text = "Name";
            this.label_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_name
            // 
            this.textBox_name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_name.Location = new System.Drawing.Point(122, 30);
            this.textBox_name.Margin = new System.Windows.Forms.Padding(10);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(318, 26);
            this.textBox_name.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 41);
            this.label1.TabIndex = 2;
            this.label1.Text = "Hitpoints";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_hp
            // 
            this.textBox_hp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_hp.Location = new System.Drawing.Point(122, 71);
            this.textBox_hp.Margin = new System.Windows.Forms.Padding(10);
            this.textBox_hp.Name = "textBox_hp";
            this.textBox_hp.Size = new System.Drawing.Size(318, 26);
            this.textBox_hp.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 41);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mana";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_armor
            // 
            this.textBox_armor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_armor.Location = new System.Drawing.Point(122, 153);
            this.textBox_armor.Margin = new System.Windows.Forms.Padding(10);
            this.textBox_armor.Name = "textBox_armor";
            this.textBox_armor.Size = new System.Drawing.Size(318, 26);
            this.textBox_armor.TabIndex = 7;
            // 
            // textBox_mana
            // 
            this.textBox_mana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_mana.Location = new System.Drawing.Point(122, 112);
            this.textBox_mana.Margin = new System.Windows.Forms.Padding(10);
            this.textBox_mana.Name = "textBox_mana";
            this.textBox_mana.Size = new System.Drawing.Size(318, 26);
            this.textBox_mana.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 44);
            this.label3.TabIndex = 6;
            this.label3.Text = "Armor";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_error
            // 
            this.label_error.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label_error, 2);
            this.label_error.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_error.ForeColor = System.Drawing.Color.DarkRed;
            this.label_error.Location = new System.Drawing.Point(3, 490);
            this.label_error.Name = "label_error";
            this.label_error.Size = new System.Drawing.Size(1093, 20);
            this.label_error.TabIndex = 2;
            this.label_error.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_error.Visible = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(463, 248);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(633, 239);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(627, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "From Database";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.label_description_database, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.cBox_chars, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 20);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.Size = new System.Drawing.Size(633, 219);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // label_description_database
            // 
            this.label_description_database.AutoSize = true;
            this.label_description_database.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_description_database.Location = new System.Drawing.Point(3, 34);
            this.label_description_database.Name = "label_description_database";
            this.label_description_database.Size = new System.Drawing.Size(627, 185);
            this.label_description_database.TabIndex = 1;
            // 
            // cBox_chars
            // 
            this.cBox_chars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cBox_chars.FormattingEnabled = true;
            this.cBox_chars.Location = new System.Drawing.Point(3, 3);
            this.cBox_chars.Name = "cBox_chars";
            this.cBox_chars.Size = new System.Drawing.Size(627, 28);
            this.cBox_chars.TabIndex = 2;
            this.cBox_chars.SelectedIndexChanged += new System.EventHandler(this.CBox_chars_SelectedIndexChanged);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.label_description, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(463, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.Size = new System.Drawing.Size(633, 239);
            this.tableLayoutPanel7.TabIndex = 4;
            // 
            // label_description
            // 
            this.label_description.AutoSize = true;
            this.label_description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_description.Location = new System.Drawing.Point(3, 20);
            this.label_description.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label_description.Name = "label_description";
            this.label_description.Size = new System.Drawing.Size(627, 214);
            this.label_description.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(627, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "From class object";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CharacterCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 510);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(1121, 566);
            this.Name = "CharacterCreation";
            this.Text = "Character Creation";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox_class.ResumeLayout(false);
            this.groupBox_class.PerformLayout();
            this.groupBox_subClass.ResumeLayout(false);
            this.groupBox_subClass.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox_hp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.TextBox textBox_mana;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox_subClass;
        private System.Windows.Forms.RadioButton radBut_sub2;
        private System.Windows.Forms.RadioButton radBut_sub1;
        private System.Windows.Forms.GroupBox groupBox_class;
        private System.Windows.Forms.RadioButton radBut_Rogue;
        private System.Windows.Forms.RadioButton radBut_Mage;
        private System.Windows.Forms.RadioButton radBut_Warr;
        private System.Windows.Forms.TextBox textBox_armor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_pwr;
        private System.Windows.Forms.Label label_pwr;
        private System.Windows.Forms.Button btn_create;
        private System.Windows.Forms.Label label_description;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label_error;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_description_database;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_insert;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.ComboBox cBox_chars;
    }
}

