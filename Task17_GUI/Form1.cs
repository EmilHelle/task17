﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
using Task17ClassLibrary.character.subtypes;

namespace Task17_GUI
{
    public partial class CharacterCreation : Form
    {
        // Directory of the project
        string projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
        Task17ClassLibrary.Character character = null;
        

        public CharacterCreation()
        {
            InitializeComponent();
            UpdateImportedCharacters();
        }

        private void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            // Display the subclasses
            radBut_sub1.Text = "Assassin";
            radBut_sub2.Text = "Ninja";
            groupBox_subClass.Visible = true;

            // Display the power bar
            label_pwr.Text = "Stealth Ability";
            tableLayoutPanel5.Visible = true;
        }

        private void Btn_create_Click(object sender, EventArgs e)
        {
            #region Input validation
            string name = "";
            int hp = 0;
            int mana = 0;
            int armor = 0;
            int pwr = 0;

            label_error.Visible = true;
            if(textBox_name.Text == "")
            {
                label_error.Text = "No name in field";
                return;
            }
            else
            {
                name = textBox_name.Text;
            }

            try
            {
                if (textBox_hp.Text == "")
                {
                    label_error.Text = "No hitpoints in field";
                    return;
                }
                else
                {
                    hp = int.Parse(textBox_hp.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for hp";
                return;
            }

            try
            {
                if (textBox_mana.Text == "")
                {
                    label_error.Text = "No mana in field";
                    return;
                }
                else
                {
                    mana = int.Parse(textBox_mana.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for mana";
                return;
            }

            try
            {
                if (textBox_armor.Text == "")
                {
                    label_error.Text = "No hitpoints in field";
                    return;
                }
                else
                {
                    armor = int.Parse(textBox_armor.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for armor";
                return;
            }

            try
            {
                if (textBox_armor.Text == "")
                {
                    label_error.Text = $"No {label_pwr.Text.ToLower()} in field";
                    return;
                }
                else
                {
                    pwr = int.Parse(textBox_pwr.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = $"Invalid value for {label_pwr.Text.ToLower()}";
                return;
            }
            #endregion

            #region Retrieve class and subclass
            string mainClass = "";
            if (radBut_Mage.Checked) { mainClass = "Mage"; }
            else if (radBut_Warr.Checked) { mainClass = "Warrior"; }
            else if (radBut_Rogue.Checked) { mainClass = "Rogue"; }


            string subClass = "";
            if (radBut_sub1.Checked) { subClass = radBut_sub1.Text; }
            else if (radBut_sub2.Checked) { subClass = radBut_sub2.Text; }

            #endregion

            #region Create class
            // Generic character for later calls
            if(mainClass == "Warrior")
            {
                if(subClass == "Barbarian")
                {
                    character = new Barbarian(pwr, name, hp, mana, armor);
                }
                else if(subClass == "Soldier")
                {
                    character = new Soldier(pwr, name, hp, mana, armor);
                }
            }
            else if (mainClass == "Mage")
            {
                if (subClass == "Druid")
                {
                    character = new Druid(pwr, name, hp, mana, armor);
                }
                else if (subClass == "Necromancer")
                {
                    character = new Necromancer(pwr, name, hp, mana, armor);
                }
            }
            else if (mainClass == "Rogue")
            {
                if (subClass == "Assassin")
                {
                    character = new Assassin(pwr, name, hp, mana, armor);
                }
                else if (subClass == "Ninja")
                {
                    character = new Ninja(pwr, name, hp, mana, armor);
                }
            }
            #endregion

            #region Print info to window, and save file
            Dictionary<string, string> charInfo = new Dictionary<string, string>
            {
                { "Name", character.Name },
                { "Class", mainClass },
                { "SubClass", subClass },
                { "HP", character.Hp.ToString() },
                { "Mana", character.Mana.ToString() },
                { "Armor", character.ArmorRating.ToString() },
                { label_pwr.Text, textBox_pwr.Text }
            };

            // Print info
            label_description.Text = "";
            foreach(KeyValuePair<string,string> s in charInfo)
            {
                label_description.Text += s.Key + ": " + s.Value + "\n";
            }

            // Save to file
            //
            using (StreamWriter sw = new StreamWriter("Character.txt"))
            {
                sw.WriteLine("Name: " + character.Name);
                sw.WriteLine("Class: " + mainClass);
                sw.WriteLine("SubClass: " + subClass);
                sw.WriteLine("HP: " + character.Hp.ToString());
                sw.WriteLine("Mana: " + character.Mana.ToString());
                sw.WriteLine("Armor: " + character.ArmorRating.ToString());
                sw.WriteLine(label_pwr.Text + ": " + textBox_pwr.Text);
            }

            // Save SQL code to file
            string sqlQuery =   $"INSERT INTO Character(Name, Main, Sub, " +
                                $"HP, Mana, Armor, Power)" +
                                $"VALUES(\"{character.Name}\", \"{mainClass}\", \"{subClass}\", {character.Hp.ToString()}, " +
                                $"{character.Mana.ToString()}, {character.ArmorRating.ToString()}," +
                                $"{textBox_pwr.Text});";

            using (StreamWriter sw = new StreamWriter("CharacterInsertSQL.txt"))
            {
                sw.Write(sqlQuery);
            }
            #endregion

            label_error.Text = "";
            label_error.Visible = false;
        }

        private void RadBut_Warr_CheckedChanged(object sender, EventArgs e)
        {
            // Display the subclasses
            radBut_sub1.Text = "Barbarian";
            radBut_sub2.Text = "Soldier";
            groupBox_subClass.Visible = true;

            // Display the power bar
            label_pwr.Text = "Strength";
            tableLayoutPanel5.Visible = true;
        }

        private void RadBut_Mage_CheckedChanged(object sender, EventArgs e)
        {
            // Display the subclasses
            radBut_sub1.Text = "Druid";
            radBut_sub2.Text = "Necromancer";
            groupBox_subClass.Visible = true;

            // Display the power bar
            label_pwr.Text = "Magic Power";
            tableLayoutPanel5.Visible = true;
        }

        private void RadBut_sub1_CheckedChanged(object sender, EventArgs e)
        {
            btn_create.Visible = true;
        }

        private void RadBut_sub2_CheckedChanged(object sender, EventArgs e)
        {
            btn_create.Visible = true;
        }

        private void Btn_insert_Click(object sender, EventArgs e)
        {
            #region Input validation
            string name = "";
            int hp = 0;
            int mana = 0;
            int armor = 0;
            int pwr = 0;

            label_error.Visible = true;
            if (textBox_name.Text == "")
            {
                label_error.Text = "No name in field";
                return;
            }
            else
            {
                name = textBox_name.Text;
            }

            try
            {
                if (textBox_hp.Text == "")
                {
                    label_error.Text = "No hitpoints in field";
                    return;
                }
                else
                {
                    hp = int.Parse(textBox_hp.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for hp";
                return;
            }

            try
            {
                if (textBox_mana.Text == "")
                {
                    label_error.Text = "No mana in field";
                    return;
                }
                else
                {
                    mana = int.Parse(textBox_mana.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for mana";
                return;
            }

            try
            {
                if (textBox_armor.Text == "")
                {
                    label_error.Text = "No hitpoints in field";
                    return;
                }
                else
                {
                    armor = int.Parse(textBox_armor.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for armor";
                return;
            }

            try
            {
                if (textBox_armor.Text == "")
                {
                    label_error.Text = $"No {label_pwr.Text.ToLower()} in field";
                    return;
                }
                else
                {
                    pwr = int.Parse(textBox_pwr.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = $"Invalid value for {label_pwr.Text.ToLower()}";
                return;
            }
            #endregion

            #region Retrieve class and subclass
            string mainClass = "";
            if (radBut_Mage.Checked) { mainClass = "Mage"; }
            else if (radBut_Warr.Checked) { mainClass = "Warrior"; }
            else if (radBut_Rogue.Checked) { mainClass = "Rogue"; }


            string subClass = "";
            if (radBut_sub1.Checked) { subClass = radBut_sub1.Text; }
            else if (radBut_sub2.Checked) { subClass = radBut_sub2.Text; }

            #endregion

            #region Insert into the database
            // Save SQL code to file
            string sqlQuery = $"INSERT INTO Character(Name, Main, Sub, " +
                                $"HP, Mana, Armor, Power)" +
                                $"VALUES(\"{name}\", \"{mainClass}\", \"{subClass}\", {hp}, " +
                                $"{mana}, {armor}, {textBox_pwr.Text});";

            try
            {
                using(SQLiteConnection conn = dbConn())
                {
                    conn.Open();
                    SQLiteCommand sqlite_cmd = conn.CreateCommand();
                    sqlite_cmd.CommandText = sqlQuery;
                    sqlite_cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                label_error.Text = ex.Message;
                return;
            }

            #endregion

            UpdateImportedCharacters();
            label_error.Text = "";
            label_error.Visible = false;
        }

        private SQLiteConnection dbConn()
        {
            SQLiteConnection conn = new SQLiteConnection($"Data Source = {projectDir}/database/characters.db;" +
                $"Version = 3;");
            return conn;
        }

        private void Btn_update_Click(object sender, EventArgs e)
        {
            #region Input validation
            string name = "";
            int hp = 0;
            int mana = 0;
            int armor = 0;
            int pwr = 0;

            label_error.Visible = true;
            if (textBox_name.Text == "")
            {
                label_error.Text = "No name in field";
                return;
            }
            else
            {
                name = textBox_name.Text;
            }

            try
            {
                if (textBox_hp.Text == "")
                {
                    label_error.Text = "No hitpoints in field";
                    return;
                }
                else
                {
                    hp = int.Parse(textBox_hp.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for hp";
                return;
            }

            try
            {
                if (textBox_mana.Text == "")
                {
                    label_error.Text = "No mana in field";
                    return;
                }
                else
                {
                    mana = int.Parse(textBox_mana.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for mana";
                return;
            }

            try
            {
                if (textBox_armor.Text == "")
                {
                    label_error.Text = "No hitpoints in field";
                    return;
                }
                else
                {
                    armor = int.Parse(textBox_armor.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = "Invalid value for armor";
                return;
            }

            try
            {
                if (textBox_armor.Text == "")
                {
                    label_error.Text = $"No {label_pwr.Text.ToLower()} in field";
                    return;
                }
                else
                {
                    pwr = int.Parse(textBox_pwr.Text);
                }
            }
            catch (FormatException)
            {
                label_error.Text = $"Invalid value for {label_pwr.Text.ToLower()}";
                return;
            }
            #endregion

            #region Retrieve class and subclass
            string mainClass = "";
            if (radBut_Mage.Checked) { mainClass = "Mage"; }
            else if (radBut_Warr.Checked) { mainClass = "Warrior"; }
            else if (radBut_Rogue.Checked) { mainClass = "Rogue"; }


            string subClass = "";
            if (radBut_sub1.Checked) { subClass = radBut_sub1.Text; }
            else if (radBut_sub2.Checked) { subClass = radBut_sub2.Text; }

            #endregion

            #region Update the database
            // Save SQL code to file
            string sqlQuery = $"UPDATE Character " +
                $"SET Name = \"{name}\", Main = \"{mainClass}\", Sub = \"{subClass}\", " +
                $"HP = {hp}, Mana = {mana}, Armor = {armor}, Power = {textBox_pwr.Text} " +
                $"WHERE Name = \"{name}\";";

            try
            {
                using (SQLiteConnection conn = dbConn())
                {
                    conn.Open();
                    SQLiteCommand sqlite_cmd = conn.CreateCommand();
                    sqlite_cmd.CommandText = sqlQuery;
                    sqlite_cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                label_error.Text = ex.Message;
                return;
            }

            #endregion

            UpdateImportedCharacters();
            label_error.Text = "";
            label_error.Visible = false;
        }

        private void Btn_delete_Click(object sender, EventArgs e)
        {
            #region Input validation
            string name = "";

            label_error.Visible = true;
            if (textBox_name.Text == "")
            {
                label_error.Text = "No name in field";
                return;
            }
            else
            {
                name = textBox_name.Text;
            }
            #endregion

            #region Update the database
            // Save SQL code to file
            string sqlQuery = $"DELETE from Character WHERE Name = \"{name}\";";

            try
            {
                using (SQLiteConnection conn = dbConn())
                {
                    conn.Open();
                    SQLiteCommand sqlite_cmd = conn.CreateCommand();
                    sqlite_cmd.CommandText = sqlQuery;
                    sqlite_cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                label_error.Text = ex.Message;
                return;
            }

            #endregion

            UpdateImportedCharacters();
            //label_error.Text = "";
            //label_error.Visible = false;
        }

        private void UpdateImportedCharacters()
        {
            // Fetch data from database
            try
            {
                using (SQLiteConnection conn = dbConn())
                {
                    SQLiteDataReader sqlite_datareader = null;
                    conn.Open();
                    SQLiteCommand sqlite_cmd;
                    sqlite_cmd = conn.CreateCommand();
                    sqlite_cmd.CommandText = "SELECT * FROM Character";
                    sqlite_datareader = sqlite_cmd.ExecuteReader();

                    // Fill the combobox
                    cBox_chars.Items.Clear();
                    while (sqlite_datareader.Read())
                    {
                        cBox_chars.Items.Add(sqlite_datareader["Name"]);
                    }
                    cBox_chars.Sorted = true;
                    cBox_chars.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                label_error.Text = ex.Message;
                return;
            }
        }

        private void CBox_chars_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Change the text depending on the comboBox
            label_error.Visible = true;
            try
            {
                using (SQLiteConnection conn = dbConn())
                {
                    SQLiteDataReader sqlite_datareader = null;
                    conn.Open();
                    SQLiteCommand sqlite_cmd;
                    sqlite_cmd = conn.CreateCommand();
                    sqlite_cmd.CommandText = $"SELECT * FROM Character WHERE Name = \"{cBox_chars.SelectedItem}\";";
                    sqlite_datareader = sqlite_cmd.ExecuteReader();

                    // Write the data
                    sqlite_datareader.Read();
                    string test = sqlite_datareader["Name"].ToString();
                    string desc = $"Name: {sqlite_datareader["Name"]}\nMain Class: {sqlite_datareader["Main"]}\n" +
                        $"Sub Class: {sqlite_datareader["Sub"]}\nHp: {sqlite_datareader["HP"].ToString()}\n" +
                        $"Mana: {sqlite_datareader["Mana"].ToString()}\nArmor: {sqlite_datareader["Armor"].ToString()}\n" +
                        $"Power: {sqlite_datareader["Power"].ToString()}";
                    label_description_database.Text = desc;
                }
            }
            catch (Exception ex)
            {
                label_error.Text = ex.Message;
                return;
            }
            label_error.Visible = false;
        }
    }
}
