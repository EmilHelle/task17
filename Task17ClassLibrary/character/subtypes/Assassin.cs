﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task17ClassLibrary.character.classes;

namespace Task17ClassLibrary.character.subtypes
{
    public class Assassin : Rogue
    {
        public Assassin(int stealthAbility, string name, int hp, int mana, int armorRating) : base(stealthAbility, name, hp, mana, armorRating)
        {
        }

        public override int Attack()
        {
            return base.Attack() + 5;
        }

        public override int Move()
        {
            return base.Move() + 5;
        }
    }
}
