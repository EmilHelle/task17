﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task17ClassLibrary.character.classes;

namespace Task17ClassLibrary.character.subtypes
{
    public class Ninja : Rogue
    {
        public Ninja(int stealthAbility, string name, int hp, int mana, int armorRating) : base(stealthAbility, name, hp, mana, armorRating)
        {
        }

        public override int Attack()
        {
            return base.Attack() + 10;
        }

        public override int Move()
        {
            return base.Move() + 10;
        }
    }
}
