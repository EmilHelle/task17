﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17ClassLibrary.character.classes
{
    public abstract class Warrior : Character
    {
        private int strength;

        public Warrior(int strength, string name, int hp, int mana, int armorRating) : base(name, hp, mana, armorRating)
        {
            this.strength = strength;
        }

        public int Strength { get => strength; set => strength = value; }

        public override int Attack()
        {
            int damageDealt = strength + 15;
            return damageDealt;
        }

        public override int Move()
        {
            int stepsMoved = Strength + 5;
            return stepsMoved;
        }
    }
}
