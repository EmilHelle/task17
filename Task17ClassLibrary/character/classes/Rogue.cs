﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17ClassLibrary.character.classes
{
    public abstract class Rogue : Character
    {
        private int stealthAbility;

        public Rogue(int stealthAbility, string name, int hp, int mana, int armorRating) : base(name, hp, mana, armorRating)
        {
            this.stealthAbility = stealthAbility;
        }

        public int StealthAbility { get => stealthAbility; set => stealthAbility = value; }

        public override int Attack()
        {
            int damageDealt = stealthAbility + 5;
            return damageDealt;
        }

        public override int Move()
        {
            int stepsMoved = StealthAbility + 10;
            return stepsMoved;
        }
    }
}
