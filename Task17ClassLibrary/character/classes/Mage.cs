﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17ClassLibrary.character.classes
{
    public abstract class Mage : Character
    {
        private int magicPower;

        public Mage(int magicPower, string name, int hp, int mana, int armorRating) : base(name, hp, mana, armorRating)
        {
            this.magicPower = magicPower;
        }

        public int MagicPower { get => magicPower; set => magicPower = value; }

        public override int Attack()
        {
            int damageDealt = magicPower + 10;
            return damageDealt;
        }

        public override int Move()
        {
            int stepsMoved = 5;
            return stepsMoved;
        }
    }
}
