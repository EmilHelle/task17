﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17ClassLibrary
{
    public abstract class Character
    {
        private string name;
        private int hp;
        private int mana;
        private int armorRating;

        public Character(string name, int hp, int mana, int armorRating)
        {
            this.name = name;
            this.hp = hp;
            this.mana = mana;
            this.armorRating = armorRating;
        }

        public string Name { get => name; set => name = value; }
        public int Hp { get => hp; set => hp = value; }
        public int Mana { get => mana; set => mana = value; }
        public int ArmorRating { get => armorRating; set => armorRating = value; }

        public abstract int Attack();

        public abstract int Move();
    }
}
